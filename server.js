var PORT = 80;

var nconf = require('nconf');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mime = require('mime');
var fs = require('fs');
var cors = require('cors');
var favicon = require('serve-favicon');
var serveStatic = require('serve-static');

global.appRoot = path.resolve(__dirname);

//var ejs = require('ejs');
//var engine = require('ejs-locals');
var engine = require('ejs-mate')

nconf.argv()
       .env()
       .file({file: 'config.json'});

nconf.defaults({
    'server': {
        'port': PORT
    }
});

var app = express();
app.use(cors());

app.engine('ejs', engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(favicon(__dirname + '/public/images/favicon.png'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(cookieParser());
app.use(session({
    secret : "SHUUUUSH",
    saveUninitialized: true,
    resave : false
}));

app.use(serveStatic('_book', {
          maxAge: '1d',
          setHeaders: setCustomCacheControl
    }
));

var index = require('./routes/index');
app.use(index);

// app.use(function(req, res, next) {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        console.log(err.stack);

        res.status(err.status || 500);
        res.json({
            "message": err.message,
            "error": err
        });
    });
}

app.use(function(err, req, res, next) {
    console.log(err.stack);

    res.status(err.status || 500);
    res.json({
        "message": err.message,
        "error": {}
    });
});

app.listen(nconf.get('server:port'), function(){
  console.log("Started on PORT " + nconf.get('server:port'));
});

function setCustomCacheControl(res, path) {
  if (serveStatic.mime.lookup(path) === 'text/html') {
    // Custom Cache-Control for HTML files
    res.setHeader('Cache-Control', 'public, max-age=0')
  }
}

// process.on('uncaughtException', function (error) {
//    console.log(error.stack);
// });
